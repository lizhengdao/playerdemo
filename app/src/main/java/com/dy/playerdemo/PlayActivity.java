package com.dy.playerdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.dy.playerdemo.player.MediaCodecPlayer;
import com.dy.playerdemo.player.PlayerVersion;

public class PlayActivity extends AppCompatActivity {

//    private static final String DEFAULT_FILE_URL = "/sdcard/Sync-One2-Test-1080p-24-H_264_V.mp4";
    private static final String DEFAULT_FILE_URL = "/sdcard/test60.mp4";

    private SurfaceView sv_preview;
    private MediaCodecPlayer mCodecPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        sv_preview = findViewById(R.id.sv_preview);
        sv_preview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                mCodecPlayer = new MediaCodecPlayer(holder, getApplicationContext(), PlayerVersion.VERSION5);
                mCodecPlayer.setAudioDataSource(Uri.parse(DEFAULT_FILE_URL), null);
                mCodecPlayer.setVideoDataSource(Uri.parse(DEFAULT_FILE_URL), null);
                mCodecPlayer.prepare();
                mCodecPlayer.start(); //from IDLE to PREPARING
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (mCodecPlayer != null) {
                    mCodecPlayer.release();
                    mCodecPlayer = null;
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mCodecPlayer != null) {
            mCodecPlayer.release();
            mCodecPlayer = null;
        }
    }
}
